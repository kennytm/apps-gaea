[InstallDelete]
Type: filesandordirs; Name: "{app}/bin"
Type: filesandordirs; Name: "{app}/data"
Type: filesandordirs; Name: "{app}/docs"
Type: filesandordirs; Name: "{app}/scripts"
Type: filesandordirs; Name: "{app}/apps"

[UninstallDelete]
Type: filesandordirs; Name: "{app}/bin"
Type: filesandordirs; Name: "{app}/data"
Type: filesandordirs; Name: "{app}/docs"
Type: filesandordirs; Name: "{app}/scripts"
Type: filesandordirs; Name: "{app}/apps"

[Setup]
ChangesAssociations=yes

[Registry]
;; ".myp" is the extension we're associating. "MyProgramFile" is the
;; internal name for the file type as stored in the registry. Make
;; sure you use a unique name for this so you don't inadvertently
;; overwrite another application's registry key.
Root: HKCR; Subkey: ".aqp"; ValueType: string; ValueName: ""; ValueData: "aglScript"; Flags: uninsdeletevalue
Root: HKCR; Subkey: ".aqw"; ValueType: string; ValueName: ""; ValueData: "aglScriptW"; Flags: uninsdeletevalue
Root: HKCR; Subkey: ".gpk"; ValueType: string; ValueName: ""; ValueData: "GaeaPackage"; Flags: uninsdeletevalue
Root: HKCR; Subkey: ".gml"; ValueType: string; ValueName: ""; ValueData: "KronosForm"; Flags: uninsdeletevalue

;; "My Program File" above is the name for the file type as shown in
;; Explorer.
Root: HKCR; Subkey: "aglScript"; ValueType: string; ValueName: ""; ValueData: "aglScript CmdLine"; Flags: uninsdeletekey
Root: HKCR; Subkey: "aglScriptW"; ValueType: string; ValueName: ""; ValueData: "aglScript Windowed"; Flags: uninsdeletekey
Root: HKCR; Subkey: "GaeaPackage"; ValueType: string; ValueName: ""; ValueData: "Gaea Package"; Flags: uninsdeletekey
Root: HKCR; Subkey: "KronosForm"; ValueType: string; ValueName: ""; ValueData: "Kronos Form"; Flags: uninsdeletekey

;; "DefaultIcon" is the registry key that specifies the filename containing the icon to associate with the file type. ",0" tells Explorer to use the first icon from MYPROG.EXE. (",1" would mean the second icon.)
Root: HKCR; Subkey: "aglScript\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}/data/icons/ts.ico,0"
Root: HKCR; Subkey: "aglScriptW\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}/data/icons/ts.ico,0"
Root: HKCR; Subkey: "GaeaPackage\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}/data/icons/player.ico,0"
Root: HKCR; Subkey: "KronosForm\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}/data/icons/kronos.ico,0"

;; "DefaultIcon" is the registry key that specifies the filename containing the icon to associate with the file type. ",0" tells Explorer to use the first icon from MYPROG.EXE. (",1" would mean the second icon.)
Root: HKCR; Subkey: "aglScript\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}/bin/nt-x86/TSPlayer_x86_ra.exe"" -no-banner -r ""%1"""
Root: HKCR; Subkey: "aglScriptW\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}/bin/nt-x86/TSPlayerw_x86_ra.exe"" -r ""%1"""
Root: HKCR; Subkey: "GaeaPackage\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}/bin/nt-x86/TSPlayerw_x86_ra.exe"" -r ""%1"""
Root: HKCR; Subkey: "KronosForm\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}/bin/nt-x86/TSPlayerw_x86_ra.exe"" -startApp ""%1"" -r ""{app}/scripts/app.aq"""

;; "shell\open\command" is the registry key that specifies the program to execute when a file of the type is double-clicked in Explorer. The surrounding quotes are in the command line so it handles long filenames correctly.

;; Multiple languages...
; [Languages]
; Name: "en"; MessagesFile: "compiler:Default.isl"
; Name: "cn"; MessagesFile: "compiler:Languages\Chinese.isl"
; [Files]
; Source:"..\..\Gaea\app\config\gaea_options.config.cn.xml"; DestDir: "{app}\apps\gaea\config"; DestName: "gaea_options.config.xml";  Languages: cn;
; Source:"..\..\Gaea\app\config\gaea_options.config.en.xml"; DestDir: "{app}\apps\gaea\config"; DestName: "gaea_options.config.xml";  Languages: en;
