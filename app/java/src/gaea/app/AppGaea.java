package gaea.app;

import java.io.IOException;

import ni.aglSystem.EOSWindowMessage;
import ni.aglSystem.IMessage;
import ni.aglSystem.Lib;
import ni.aglUI.EUIMessage;
import ni.aglUI.IWidget;
import selena.SelenaException;
import selena.app.AppConfig;
import selena.app.AppEventHandler;
import selena.app.BaseAppWindow;
import selena.app.script.ScriptException;
import selena.app.script.VM;
import selena.app.util.Files;
import selena.app.util.NewInstances;

class AppGaea implements AppEventHandler {
    @Override
    public void onInitConfig(final BaseAppWindow appWnd, final AppConfig cfg) {
        cfg.windowTitle = "Gaea";
        cfg.drawFps = true;
        // Gaea is "development" mode so we want new instance to always reload from the latest class file
        NewInstances.ALWAYS_RELOAD = true;
    }

    @Override
    public void onShutdown(final BaseAppWindow appWnd) {
        try {
            final VM vm = appWnd.getScriptVM();
            if (vm != null) {
                vm.run(null, "::app.shutdown()");
            }
        }
        catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean onWindowMessage(final BaseAppWindow appWindow, final IMessage msg) {
        if (msg.getID() == EOSWindowMessage.Close) {
            try {
                appWindow.getScriptVM().run(null, "::app.tryClose()");
            }
            catch (final ScriptException e) {
                // Not handled... or error, we just exit
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean onDesktopMessage(final BaseAppWindow wnd, final IWidget w, final int msg, final Object a, final Object b) throws IOException, SelenaException
    {
        if (msg == EUIMessage.SinkAttached) {
            final VM vm = wnd.getScriptVM();
            vm.run(null, Files.openRead("gaea.aqw"));
            vm.run(null, "::main()");
        }
        return false;
    }

    @Override
    public boolean onStartup(final BaseAppWindow appWindow) {
        return true;
    }

    @Override
    public boolean onRootMessage(final BaseAppWindow appWindow, final IWidget w, final int msg, final Object a, final Object b) throws Exception
    {
        return false;
    }

    public static void main(final String[] args) {
        Lib.loadModule("Kronos");
        selena.app.App.run(new AppGaea());
    }
}
